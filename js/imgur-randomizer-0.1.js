var maxImagesInCache = 600;
var rows             = 3;
var columns          = 3;
var imageTimeout     = 50;
var cacheImages      = false;
var imageCache       = [];
var resumeCaching    = maxImagesInCache/2;
var imagesPerPage    = columns*rows;

function randomString(stringLength) {
    var chars  = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    var output = "";
    for (var i = 0; i < stringLength; i++) {
        output += chars.substr(Math.floor(Math.random() * chars.length), 1);
    };
    return output;
};

function showImages(current) {
    currentRow    = Math.floor(((imagesPerPage - current) - 1 ) / columns);
    currentColumn = (imagesPerPage - current - 1) % columns;
    if(imageCache.length < 0) {
        return;
    };
    var url = imageCache.shift();
    var imageTag = '<a href="' + url + '"><img src="' + url + '"/></a>';
    $('tr:eq(' + currentRow + ') > td:eq(' + currentColumn + ')').append(imageTag);
    if(current == 0) {
        $(".imagearea").fadeIn();
    };
    if(imageCache.length < resumeCaching && cacheImages == false) {
        cacheImages = true;
        getImages();
    };
};

function createTable() {
    for (var i = rows; i >= 1; i--) {
        $('.imagearea').append('<tr></tr>');
    };
    for (var i = columns; i >= 1; i--) {
        $('tr').append('<td></td>');
    };
};

function getImages() {
    if(imageCache.length >= maxImagesInCache) {
        cacheImages = false;
    } else if(cacheImages == true) {
        var imgObject = new Image();
        imgObject.onload = function() {
            // If the image is too small, try again.
            if (this.width < 500 || this.height < 500) {
                this.src = "http://i.imgur.com/" + randomString(5) + ".png";
                console.log("Failed...");
            }
            // If the right size, run getImages again.
            else {
                imageCache.push(imgObject.src);
                updateCounters();
                console.log("Success!");
                setTimeout("getImages()", imageTimeout);
            }
        }
    imgObject.src = "http://i.imgur.com/" + randomString(5) + ".png";
    } else {
        return;
    };
};

function updateCounters() {
    var imageCount   = imageCache.length;
    var imagePercent = imageCount/maxImagesInCache*100;
    $(".imageCount").text(imageCount + "/" + maxImagesInCache);
    $(".progressBar").progressbar({
        value: imagePercent
    });
    $(".maxCacheCount").text(maxImagesInCache);
    $(".rowsCount").text(rows);
    $(".columnsCount").text(columns);
    updateButton();
};

function populateImageArea() {
    $(".imagearea").hide();
    $(".imagearea").empty();
    createTable();
    for (var i = imagesPerPage - 1; i >= 0; i--) {
        showImages(i);
    }
};

function updateButton() {
    if(imageCache.length >= imagesPerPage) {
        $("#button_loadImages").button( "option", "disabled", false );
    }
    else {
        $("#button_loadImages").button( "option", "disabled", true );
    }
};

function initializeUI() {
    $("#loadImages").hide();
    $("#cacheSize").hide();
    $(".imagearea").hide();
    $( "#button_startLoading" ).button({
        disabled: false 
    });
    $("#button_loadImages").button({
        disabled: true
    });
    $('.options_max_cache').slider({
        max: 1000,
        step: 10,
        value: maxImagesInCache,
        slide: function(event, ui) {
            maxImagesInCache = ui.value;
            imagesPerPage = columns*rows;
            updateCounters();
        }
    });
    $(".options_rows").slider({
        min: 1,
        max: 10,
        value: rows,
        slide: function(event, ui) {
            rows = ui.value;
            imagesPerPage = columns*rows;
            updateCounters();
        }
    });
    $(".options_columns").slider({
        min: 1,
        max: 10,
        value: columns,
        slide: function(event, ui) {
            columns = ui.value;
            imagesPerPage = columns*rows;
            updateCounters();
        }
    });
    $(".progressBar").progressbar({
        value: 0
    });
    updateCounters();
};

$(document).ready(function() {
    cacheImages = false;
    initializeUI();
    $("#button_startLoading").click(function() {
        cacheImages = true;
        getImages();
        $("#startLoading").effect('blind', 100);
        $("#options").effect('blind', 100);
        $("#loadImages").fadeIn(100);
        $("#cacheSize").fadeIn(100);
    });
    $("#button_loadImages").click(function() {
        cacheImages = false; // might not be needed
        populateImageArea();
        updateCounters();
    });
});